echo "Installing requirements"
pip install -r requirements.txt

echo "Creating environment"
mkdir -p tmp

echo "Applying alembic database migrations"
flask3/bin/alembic upgrade head
