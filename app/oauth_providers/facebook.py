__author__ = 'mauro'

import os
from flask import request, session, redirect, url_for
from flask_oauthlib.client import OAuthException
from flask_login import login_user
from app import app, oauth, models, db


FACEBOOK_APP_ID = os.environ.get('__FACEBOOK_APP_ID', 'key')
FACEBOOK_APP_SECRET = os.environ.get('__FACEBOOK_APP_SECRET', 'secret')

facebook_oauth = oauth.remote_app(
    'facebook',
    consumer_key=FACEBOOK_APP_ID,
    consumer_secret=FACEBOOK_APP_SECRET,
    request_token_params={'scope': 'email'},
    base_url='https://graph.facebook.com',
    request_token_url=None,
    access_token_url='/oauth/access_token',
    authorize_url='https://www.facebook.com/dialog/oauth'
)


@app.route('/login/facebook')
def facebook_login_oauth():
    callback = url_for(
        'facebook_authorized_handler',
        next=request.args.get('next') or request.referrer or None,
        _external=True
    )
    return facebook_oauth.authorize(callback=callback)


@app.route('/login/facebook/authorized')
@facebook_oauth.authorized_handler
def facebook_authorized_handler(resp):
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    if isinstance(resp, OAuthException):
        return 'Access denied: %s' % resp.message

    session['oauth_token'] = (resp['access_token'], '')
    me = facebook_oauth.get('/me')
    email = me.data['email']
    user_model = models.User().query.filter_by(email=email).first()
    if user_model is None:
        nickname = email.split('@')[0]
        nickname = models.User.make_unique_nickname(nickname)
        user_model = models.User(nickname=nickname, email=email, role=models.ROLE_USER)
        db.session.add(user_model)
        # make the user follow him/herself
        db.session.add(user_model.follow(user_model))
        db.session.commit()
    remember_me = False
    if 'remember_me' is session:
        remember_me = session['remeber_me']
        session.pop('remember_me', None)
    login_user(user_model, remember=remember_me)
    return redirect(request.args.get('next') or url_for('index'))


@facebook_oauth.tokengetter
def tokengetter():
    return session.get('oauth_token')
