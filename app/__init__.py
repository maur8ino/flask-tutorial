import os

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.openid import OpenID
from flask_oauthlib.client import OAuth

from config import basedir

app = Flask(__name__)
app.config.from_object('config')
# SQLAlchemy
db = SQLAlchemy(app)
# LoginManager
lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
# OpenID
oid = OpenID(app, os.path.join(basedir, 'tmp'))
# OAuth
oauth = OAuth(app)

if not app.debug:
    import time
    import logging
    from logging.handlers import RotatingFileHandler

    logging.Formatter.converter = time.gmtime
    file_handler = RotatingFileHandler('tmp/microblog.log', 'a', 1 * 1024 * 1024, 10)
    file_handler.setFormatter(
        logging.Formatter('%(asctime)s GMT %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('microblog startup')

from app import views, models
